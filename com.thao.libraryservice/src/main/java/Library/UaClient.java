package Library;

import static com.google.common.collect.Lists.newArrayList;
import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.uint;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.config.OpcUaClientConfig;
import org.eclipse.milo.opcua.sdk.client.api.nodes.VariableNode;
import org.eclipse.milo.opcua.sdk.client.api.subscriptions.UaMonitoredItem;
import org.eclipse.milo.opcua.sdk.client.api.subscriptions.UaSubscription;
import org.eclipse.milo.opcua.stack.client.UaTcpStackClient;
import org.eclipse.milo.opcua.stack.core.AttributeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.ByteString;
import org.eclipse.milo.opcua.stack.core.types.builtin.DataValue;
import org.eclipse.milo.opcua.stack.core.types.builtin.DateTime;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.QualifiedName;
import org.eclipse.milo.opcua.stack.core.types.builtin.Variant;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger;
import org.eclipse.milo.opcua.stack.core.types.enumerated.MonitoringMode;
import org.eclipse.milo.opcua.stack.core.types.enumerated.TimestampsToReturn;
import org.eclipse.milo.opcua.stack.core.types.structured.EndpointDescription;
import org.eclipse.milo.opcua.stack.core.types.structured.HistoryReadDetails;
import org.eclipse.milo.opcua.stack.core.types.structured.HistoryReadResponse;
import org.eclipse.milo.opcua.stack.core.types.structured.HistoryReadResult;
import org.eclipse.milo.opcua.stack.core.types.structured.HistoryReadValueId;
import org.eclipse.milo.opcua.stack.core.types.structured.MonitoredItemCreateRequest;
import org.eclipse.milo.opcua.stack.core.types.structured.MonitoringParameters;
import org.eclipse.milo.opcua.stack.core.types.structured.ReadRawModifiedDetails;
import org.eclipse.milo.opcua.stack.core.types.structured.ReadValueId;


public class UaClient {
	
	private static UaClient INSTANCE = null;
	
	private OpcUaClient opcuaClient = null;
	private boolean connectStatus;
	private String endPoint = null;
	private final AtomicLong clientHandles = new AtomicLong(1L);	
	private UaClient(){}
	
	public static UaClient getInstance(){
		if(INSTANCE == null) {
			INSTANCE = new UaClient();
		}
		return INSTANCE;
	}
	
	public void init(String endPoint) throws Exception {
		
        EndpointDescription[] endpoints;
        this.endPoint= endPoint;
        
        try{
            endpoints = UaTcpStackClient
                    .getEndpoints(endPoint)
                    .get();  
            OpcUaClientConfig config = OpcUaClientConfig.builder().setEndpoint(endpoints[0])            		
            		.build();

            opcuaClient = new OpcUaClient(config);
        	opcuaClient.connect().get();
        	System.out.println("Opc Ua server connected");
        	connectStatus= true;
        	
        }
        catch(Throwable e){
        	connectStatus=false;
        	System.out.println("not connected: "+e.getMessage());        	
        }
		
	}
	
	public void write(int value,int id, String identifier) throws Exception {
		
		try {
			if(connectStatus==false){			
				init(endPoint);			
			}
			Variant v = new Variant(value);
			DataValue dv = new DataValue(v);
			NodeId nodeID= new NodeId(id,identifier);	
			if(opcuaClient == null) {
				connectStatus = false;
				throw new Exception("OPCUA Server not available");
			}
			opcuaClient.writeValue(nodeID, dv).get();
		} catch (Exception e) {
			connectStatus=false;
			throw e;
		}
		
    }
	
	public int read (int id, String folderName) throws Exception{
		opcuaClient.connect().get();
		NodeId nodeID =new NodeId(id, folderName);		
        VariableNode vn= opcuaClient.getAddressSpace().createVariableNode(nodeID);
        DataValue dv = vn.readValue().get();
        Variant v = dv.getValue();
        int value= (int)v.getValue();
        System.out.println(value);
        return value;
			
	}
	
	public void subscribe (int index, String identifier) throws Exception{
		
		try{
			opcuaClient.connect().get();
			UaSubscription subscription = opcuaClient.getSubscriptionManager().createSubscription(1000.0).get();
			NodeId nodeID = new NodeId(index, identifier);
			      
	        ReadValueId readValueId = new ReadValueId(
	            nodeID,
	            AttributeId.Value.uid(), null, null);

	        
	        UInteger clientHandle = uint(clientHandles.getAndIncrement());

	        MonitoringParameters parameters = new MonitoringParameters(
	            clientHandle,
	            1000.0,     // sampling interval
	            null,       // filter, null means use default
	            uint(10),   // queue size
	            true        // discard oldest
	        );

	        MonitoredItemCreateRequest request = new MonitoredItemCreateRequest(
	            readValueId, MonitoringMode.Reporting, parameters);
	        
	        BiConsumer<UaMonitoredItem, Integer> onItemCreated =
	            (item, id) -> item.setValueConsumer((arg0, arg1) -> {
					try {
						onSubscriptionValue(arg0, arg1);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				});
	           	
	        List<UaMonitoredItem> items = subscription.createMonitoredItems(
	            TimestampsToReturn.Both,
	            newArrayList(request),
	            onItemCreated
	        ).get();
		}
		catch(Throwable e){
        	connectStatus=false;
        	System.out.println("not connected: "+e.getMessage());        	
        }
		
		
	}
	
	public void history(int index, String identifier) throws Exception{
		 opcuaClient.connect().get();
		 			 	
	        HistoryReadDetails historyReadDetails = new ReadRawModifiedDetails(
	            false,
	            DateTime.MIN_VALUE,
	            DateTime.now(),
	            uint(0),
	            true
	        );

	        HistoryReadValueId historyReadValueId = new HistoryReadValueId(
	            new NodeId(index, identifier),
	            null,
	            QualifiedName.NULL_VALUE,
	            ByteString.NULL_VALUE
	        );

	        List<HistoryReadValueId> nodesToRead = new ArrayList<>();
	        nodesToRead.add(historyReadValueId);

	        HistoryReadResponse historyReadResponse = opcuaClient.historyRead(
	            historyReadDetails,
	            TimestampsToReturn.Both,
	            false,
	            nodesToRead
	        ).get();	
	        
	        HistoryReadResult[] historyReadResults = historyReadResponse.getResults();
	        System.out.println(historyReadResponse.getTypeId());
	        if	(historyReadResults == null){
	        	System.out.println("no history");
	        }
	        if (historyReadResults != null) {
	        	System.out.println(historyReadResults[0]);
	            HistoryReadResult historyReadResult = historyReadResults[0];
	           
	            System.out.println(historyReadResult.getHistoryData());
//	            HistoryData historyData = historyReadResult.getHistoryData().decode();
//	            
//	            List<DataValue> dataValues = l(historyData.getDataValues());
//
//	            dataValues.forEach(v -> System.out.println("value=" + v));
	        }
	}
		
	
	private static void onSubscriptionValue(UaMonitoredItem item, DataValue value) throws Exception {
		
		String destination =item.getReadValueId().getNodeId().getIdentifier().toString();
		if(value.getValue().getValue() != null){
		Integer v=  Integer.parseInt(value.getValue().getValue().toString());
        System.out.println( "subcripton value received:"+destination+":"+v);
        StompClient.getInstance().sendData("ws://localhost:8080/opc-ua", "/"+destination,v);     
       
		}
		
		
//        System.out.println(od.getType() +":"+ od.getQuantity());
//        return od;
//		StompClient.getInstance().sendData("Library/Books/Novel", od);
    }
	
	public void disconnect() throws InterruptedException, ExecutionException {
        opcuaClient.disconnect().get();
	}
	
	public boolean conStatus(){
		return connectStatus;
	}
	
	

}

