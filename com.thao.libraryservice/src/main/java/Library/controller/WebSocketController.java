package Library.controller;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
@Controller
public class WebSocketController {
	
	@MessageMapping("/Novel")
    @SendTo("/library/novel")
    public int novelData(int data) throws Exception {
		System.out.println("sending novel data" + data);
		
        return data;       
        
    }
	
	@MessageMapping("/History")
    @SendTo("/library/history")
    public int historyData(int data) throws Exception {
		System.out.println("sending history data" + data);
        return data;                
    }
	
	@MessageMapping("/Magazine")
    @SendTo("/library/magazine")
    public int magazineData(int data) throws Exception {
		System.out.println("sending magazine data" + data);
		return data;              
    }
	
	@MessageMapping("/Sport")
    @SendTo("/library/sport")
    public int sportData(int data) throws Exception {
		System.out.println("sending sport data" + data);
		return data;                
    }
	
	@MessageMapping("/Travel")
    @SendTo("/library/travel")
    public int travelData(int data) throws Exception {
		System.out.println("sending travel data" + data);
		return data;               
    }
	
	@MessageMapping("/Education")
    @SendTo("/library/education")
    public int educationData(int data) throws Exception {
		System.out.println("sending education data" + data);
		return data;             
    }

}
