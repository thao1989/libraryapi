package Library.controller;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import Library.UaClient;
import Library.model.Booklist;
import Library.model.OpcuaDataList;
import Library.service.IBookService;


@CrossOrigin
@RestController
public class BookController {
	
	@Autowired
	IBookService bookService;
	
	@RequestMapping("/booklist")
	public Booklist listOfBooks() throws IOException{						
		Booklist booklist = bookService.getListOfBooks();			
		return booklist;
	}
	
	@RequestMapping("/search")
	public Booklist searchForBooks(@RequestParam(value="writer", required=false) String sbw,
			                       @RequestParam(value="title", required=false) String sbt,
			                       @RequestParam(value="id", required=false) String sbid,
			                       @RequestParam(value="quantity", required=false) Integer sbq,
			                       @RequestParam(value="type", required=false) String sbtp) throws IOException{
		
		Booklist bookSearch= bookService.searchForBooks(sbw,sbt,sbid,sbq,sbtp);
		return bookSearch;
	}
	
	@RequestMapping("/insert")
	public void insertBooks(@RequestParam(value="writer") String isbw,
			                @RequestParam(value="title") String isbt,
			                @RequestParam(value="id") String isbid,          
			                @RequestParam(value="quantity") Integer isbq,
			                @RequestParam(value="type") String isbtp) throws IOException{
		bookService.insertBooks(isbw, isbt, isbid, isbq, isbtp);
		
	}
	
	@RequestMapping("/delete")
	public void deleteBooks(@RequestParam(value="id", required=false) String dbid,
							@RequestParam(value="title", required=false) String dbt,
                            @RequestParam(value="writer", required=false) String dbw,          
                            @RequestParam(value="type", required=false) String dbtp,
                            @RequestParam(value="quantity", required=false) Integer dbq
                            ) throws IOException{
		bookService.deleteBooks(dbid, dbt, dbw, dbtp, dbq);
	}
	
	@RequestMapping("/update")
	public void updateBooks(@RequestParam(value="id") String id,			                
							@RequestParam(value="newtitle", required=false) String ubt,
                            @RequestParam(value="newwriter", required=false) String ubw,          
                            @RequestParam(value="newtype", required=false) String ubtp,
                            @RequestParam(value="newquantity", required=false) Integer ubq
                            ) throws Exception{
		bookService.updateBooks(id, ubt, ubw, ubtp, ubq);
	}
	
	@RequestMapping("/searchlibrary")
	public Booklist searchLibrary(@RequestParam(value="keyword") String kw) throws IOException{

            Booklist bookSearch= bookService.searchLibrary(kw);
            return bookSearch;
    }
	
	@RequestMapping("/searchtype")
	public Booklist searchType(@RequestParam(value="type") String st) throws IOException{

            Booklist bookSearch= bookService.searchType(st);
            return bookSearch;
    }
	
	@RequestMapping("/booktype")
	public OpcuaDataList bookType() throws IOException{		
		OpcuaDataList odl = bookService.bookType();		
		return odl;
	}
	
	
	@RequestMapping("/disconnect")
	public void disconnect() throws Exception{		
		
		UaClient.getInstance().disconnect();
		
	}
	
	@RequestMapping("/connectionstatus")
	public boolean conStatus() throws IOException{						
		return bookService.conStatus();
	}

}
