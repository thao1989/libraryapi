package Library.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

import Library.model.OpcuaData;


public class MyStompSessionHandler extends StompSessionHandlerAdapter {
	
	OpcuaData od = new OpcuaData();
    private Logger logger = LogManager.getLogger(MyStompSessionHandler.class);

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        
//    	logger.info("New session established : " + session.getSessionId());
    	System.out.println("Web socket connected");
//        session.subscribe("/topic/messages", this);
//        logger.info("Subscribed to /topic/messages");
    	
    	
//    	session.send("/app/novel",5);
    	
//        logger.info("Message sent to websocket server");
    	
			
	       
	       
		
        
    }

    @Override
    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        logger.error("Got an exception", exception);
    }

   
}
