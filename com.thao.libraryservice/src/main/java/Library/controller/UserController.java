package Library.controller;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import Library.model.Userlist;
import Library.service.IUserService;

@CrossOrigin
@RestController
public class UserController {
	@Autowired
    IUserService userService;
	
	@RequestMapping("/userlist")
	public Userlist getListOfUsers() throws IOException {
		Userlist ul = userService.getListOfUsers();
		return ul;
	}
	
	@RequestMapping("/login")
	public Userlist userLogin(@RequestParam(value="userid") String lid,
			                  @RequestParam(value="password") String lpw) throws IOException{		
		Userlist userLogin= userService.userLogin(lid,lpw);
		return userLogin;
	}

}
