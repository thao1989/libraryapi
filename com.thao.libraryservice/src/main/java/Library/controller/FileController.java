package Library.controller;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import Library.model.Filelist;
import Library.service.IFileService;




public class FileController {
	
	@Autowired
	IFileService fileService;
	
	@RequestMapping("/filelist")
	public Filelist listOfFile() throws IOException{		
		Filelist filelist = fileService.getListOfFile();		
		return filelist;
	}
	
	@RequestMapping("/filesearch")
	public Filelist searchForFile(@RequestParam(value="writer", required=false) String sbw,
			                       @RequestParam(value="title", required=false) String sbt) throws IOException{
		
		Filelist fileSearch= fileService.searchForFile(sbw,sbt);
		return fileSearch;
	}

}
