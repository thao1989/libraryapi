package Library.controller;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import Library.model.Commentlist;
import Library.service.ICommentService;

@CrossOrigin
@RestController
public class CommentController {
	@Autowired
    ICommentService commentService;
	
	@RequestMapping("/commentlist")
	public Commentlist getListOfComments() throws IOException {
		Commentlist cl = commentService.getListOfComments();
		return cl;
	}
	
	@RequestMapping("/submitcomment")
	public void insertComments(@RequestParam(value="userid") String iuid,
                               @RequestParam(value="content") String ic) throws IOException{
		commentService.insertComments(iuid, ic);	
	}
	
	
	
}
