package Library;


import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import Library.controller.MyStompSessionHandler;

/**
 * Stand alone WebSocketStompClient.
 *
 */
public class StompClient {
	
private static StompClient INSTANCE = null;
	
	
	private WebSocketStompClient stompClient = null;
	private StompSession ss;
	private StompClient(){}
	public static StompClient getInstance(){
		if(INSTANCE == null) {
			INSTANCE = new StompClient();
		}
		return INSTANCE;
	}
    
    public void init(String url) throws Exception {
    	WebSocketClient webSocketClient = new StandardWebSocketClient();
        stompClient = new WebSocketStompClient(webSocketClient);

        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        
        StompSessionHandler sessionHandler = new MyStompSessionHandler();
        ss =  stompClient.connect(url, sessionHandler).get();
        
//        new Scanner(System.in).nextLine(); // Don't close immediately.
        
        System.out.println("Stomp client connected");
    }
    
    public void sendData(String url, String address,  int data) throws Exception{    
    	
//ss = stompClient.connect(url, new MyStompSessionHandler()).get();
			if(ss.isConnected()){
				ss.send(address, data);
			}
			else{
				ss=stompClient.connect(url,new MyStompSessionHandler()).get();
				ss.send(address, data);
			}
			
			
//	    	System.out.println("data sent:"+address+":"+data);
		
    	
    }
}