package Library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class Application {

    public static void main(String[] args) throws Exception {
    	
    	SpringApplication.run(Application.class, args);
        UaClient.getInstance().init("opc.tcp://edesign107:12686");        
        UaClient.getInstance().subscribe(2, "Library/Books/Books");
        UaClient.getInstance().subscribe(2, "Library/Books/Types");    
        UaClient.getInstance().subscribe(2, "Library/Books/Novel");
        UaClient.getInstance().subscribe(2, "Library/Books/History");
        UaClient.getInstance().subscribe(2, "Library/Books/Travel");
        UaClient.getInstance().subscribe(2, "Library/Books/Education");
        UaClient.getInstance().subscribe(2, "Library/Books/Magazine");
        UaClient.getInstance().subscribe(2, "Library/Books/Sport");
        StompClient.getInstance().init("ws://localhost:8080/opc-ua");
//        StompClient.getInstance().sendData("ws://localhost:8080/opc-ua","/Library/Books/Novel",5);          
        
    }
    

}