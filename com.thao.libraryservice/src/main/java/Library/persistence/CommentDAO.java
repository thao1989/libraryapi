package Library.persistence;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import Library.model.Comment;
import Library.model.Commentlist;


@Repository
public class CommentDAO {
	
	@Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String SQL = "select tbl_comment.comment_id, tbl_user.user_name, tbl_book.title, tbl_comment.content, tbl_comment.post_time from tbl_comment inner join tbl_user on tbl_comment.user_id=tbl_user.user_id inner join tbl_book on tbl_comment.book_id=tbl_book.id";
  
    public Commentlist getListOfComments(){
   	 ArrayList<Comment> comments = new ArrayList<Comment>();
   	 
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(SQL);

        for (Map<String, Object> row : rows) {
       	
             Comment c = new Comment();   
             c.setCommentID((String)row.get("comment_id"));
             c.setUserName((String)row.get("user_name"));
             c.setBookTitle((String)row.get("title"));
             c.setContent((String)row.get("content"));
             c.setPostTime(((Timestamp)row.get("post_time")));
             comments.add(c);      	 
         }       
        Commentlist cl=new Commentlist();
        cl.setCommentList(comments);
       return cl;
    }
    
//    public String generateNewID() {
//    	ArrayList<Comment> comments = new ArrayList<Comment>();
//      	String lastRow= "SELECT * FROM tbl_comment ORDER BY comment_no DESC LIMIT 1";
//        List<Map<String, Object>> rows = jdbcTemplate.queryForList(lastRow);
//        for (Map<String, Object> row : rows) {      	
//             Comment c = new Comment();             
//             c.setCommentID((String)row.get("comment_id"));
//             c.setUserID((String)row.get("user_id"));
//             c.setContent((String)row.get("content"));
//             c.setPostTime((Timestamp)row.get("post_time"));
//             comments.add(c);               
//         }      
//        String [] array = comments.toArray(new String[5]);  
//        int nn= Integer.parseInt(array[4]);
//        String newID= "C"+(nn+1)+"";
//        return newID;
//    }
    
    public void insertComments(String iuid,
				               String ic){    	
		String insert="insert into tbl_comment(comment_id, user_id, content) value(uuid(),?,?)";
		jdbcTemplate.update(insert,iuid,ic);
	
	}
 
}
