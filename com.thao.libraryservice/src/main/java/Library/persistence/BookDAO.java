package Library.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import Library.UaClient;
import Library.model.Book;
import Library.model.Booklist;
import Library.model.OpcuaData;
import Library.model.OpcuaDataList;


@Repository
public class BookDAO {

     @Autowired
     private JdbcTemplate jdbcTemplate;

     private static final String SQL = "select * from tbl_book";

     public Booklist getListOfBooks() {
        return searchBooks(null,null,null,null,null);        
     }
     
     public Booklist searchForBooks(String sbw, 
    		                        String sbt, 
    		                        String sbid,
    		                        Integer sbq,
    		                        String sbtp) {
    	 return searchBooks(sbw, sbt, sbid, sbq, sbtp);
    	 
     }
     
     public Booklist searchBooks(String filterByWriter, 
    		                     String filterByTittle, 
    		                     String filterByID,
    		                     Integer filterByQuantity,
    		                     String filterByType){
    	 ArrayList<Book> books = new ArrayList<Book>();
    	 
         List<Map<String, Object>> rows = jdbcTemplate.queryForList(SQL);

         for (Map<String, Object> row : rows) 
         {
        	 if( (filterByWriter==null||filterByWriter.equals((String)row.get("writer")))        &&
        		 (filterByTittle==null||filterByTittle.equals((String)row.get("title")))        &&
        		 (filterByID == null||filterByID.equals((String)row.get("id")))                 &&
        		 (filterByQuantity==null||filterByQuantity.equals((Integer)row.get("quantity"))) &&
        		 (filterByType==null||filterByType.equals((String)row.get("type")))){
              Book b = new Book();
              b.setTitle((String)row.get("title"));
              b.setWriter((String)row.get("writer"));
              b.setId((String)row.get("id"));
              b.setQuantity((Integer)row.get("quantity"));
              b.setType((String)row.get("type"));
              books.add(b);
        	 }
          }
         
         Booklist bl=new Booklist();
         bl.setBookList(books);
        return bl;
     }
     
     public void insertBooks(String insertID,
    		                 String insertTitle,
    		                 String insertWriter,
    		                 String insertType,
    		                 Integer insertQuantity){
    	 String insert="insert into tbl_book(id,title,writer,type,quantity) value(?,?,?,?,?)";
        jdbcTemplate.update(insert,insertID,insertTitle,insertWriter,insertType,insertQuantity);
       
     }
     
     public void deleteBooks(String deleteID,
                             String deleteTitle,
                             String deleteWriter,
                             String deleteType,
                             Integer deleteQuantity){
    	
	        String deleteByID;
	        String deleteByTitle;
	        String deleteByWriter;
	        String deleteByType;
	        String deleteByQuantity;
	        
	        if(deleteID!=null){
	        	deleteByID="DELETE FROM tbl_book WHERE ID = '"+deleteID+"' ";
	        	jdbcTemplate.update(deleteByID);
	        }
	        
	        if(deleteTitle!=null){
	        	deleteByTitle="DELETE FROM tbl_book WHERE title = '"+deleteTitle+"' ";
	        	jdbcTemplate.update(deleteByTitle);
	        }
	        
	        if(deleteWriter!=null){
	        	deleteByWriter="DELETE FROM tbl_book WHERE writer = '"+deleteWriter+"' ";
	        	jdbcTemplate.update(deleteByWriter);
	        }
	        
	        if(deleteType!=null){
	        	deleteByType="DELETE FROM tbl_book WHERE type = '"+deleteType+"' ";
	        	jdbcTemplate.update(deleteByType);
	        }
	        
	        if(deleteQuantity!=null){
	        	deleteByQuantity="DELETE FROM tbl_book WHERE quantity = '"+deleteQuantity+"' ";
	        	jdbcTemplate.update(deleteByQuantity);
	        }
	        

     }
     
	 public void updateBooks(String ID,	    		 				 
	             			 String updateTitle,
	                         String updateWriter,
	                         String updateType,
	                         Integer updateQuantity){
	
	    	
			String updateByTitle;
			String updateByWriter;
			String updateByType;
			String updateByQuantity;
			
			
			
			if(updateWriter!=null){
	        	updateByWriter="UPDATE tbl_book SET writer ='"+updateWriter+"' WHERE id = '"+ID+"' ";
	        	jdbcTemplate.update(updateByWriter);
	        }
			
			if(updateTitle!=null){
	        	updateByTitle="UPDATE tbl_book SET title ='"+updateTitle+"' WHERE id = '"+ID+"' ";
	        	jdbcTemplate.update(updateByTitle);
	        }
			
			if(updateType!=null){
	        	updateByType="UPDATE tbl_book SET type='"+updateType+"' WHERE id = '"+ID+"' ";
	        	jdbcTemplate.update(updateByType);
	        }
			
			if(updateQuantity!=null){
	        	updateByQuantity="UPDATE tbl_book SET quantity='"+updateQuantity+"' WHERE id = '"+ID+"' ";
	        	jdbcTemplate.update(updateByQuantity);
	        }	        	
	 }
	 
     public Booklist searchLibrary(String kw) {
           ArrayList<Book> books = new ArrayList<Book>();
           String search;
           
        	   search="select * from tbl_book where title like '%" + kw + "%' or writer like '%" + kw + "%'";   
          
           List<Map<String, Object>> rows1 = jdbcTemplate.queryForList(search);
           
           for (Map<String, Object> row : rows1) 
           {          	 
                Book b = new Book();
                b.setTitle((String)row.get("title"));
                b.setWriter((String)row.get("writer"));
                b.setId((String)row.get("id"));
                b.setQuantity((Integer)row.get("quantity"));
                b.setType((String)row.get("type"));
                books.add(b);
          	 
            }	
			Booklist bl=new Booklist();
			bl.setBookList(books);
			return bl;
	 }
     
     public Booklist searchType(String st) {
         ArrayList<Book> books = new ArrayList<Book>();
         String search= null;
	        search="select * from tbl_book where type like '"+ st +"'";	
         List<Map<String, Object>> rows1 = jdbcTemplate.queryForList(search);
         
         for (Map<String, Object> row : rows1) 
         {          	 
              Book b = new Book();
              b.setTitle((String)row.get("title"));
              b.setWriter((String)row.get("writer"));
              b.setId((String)row.get("id"));
              b.setQuantity((Integer)row.get("quantity"));
              b.setType((String)row.get("type"));
              books.add(b);
        	 
          }	
			Booklist bl=new Booklist();
			bl.setBookList(books);
			return bl;
	 }
     
     public int sumOfBooks(){
    	 
    	 String query="select count(id) from tbl_book";    	     	 
    	 int quantity= jdbcTemplate.queryForObject(query,Integer.class);
    	 return quantity;
    	 
     }     
     
     public int typesOfBooks(){
    	 
    	 String query="select count(distinct type) as types  from tbl_book;";    	     	 
    	 int types= jdbcTemplate.queryForObject(query,Integer.class);
    	 return types;
    	 
     } 
     
     public OpcuaDataList bookTypes(){
    	 ArrayList<OpcuaData> od = new ArrayList<OpcuaData>();
         String search= "select type, count(type) from tbl_book group by type";	        
         List<Map<String, Object>> rows1 = jdbcTemplate.queryForList(search);
         
         for (Map<String, Object> row : rows1) 
         {          	 
              OpcuaData o = new OpcuaData();
              o.setQuantity((int)((long)row.get("count(type)")));
              o.setType((String)row.get("type"));             
              od.add(o);
        	 
          }	
         	OpcuaDataList odl=new OpcuaDataList();
			odl.setOpcuaDataList(od);
			return odl;
     }
     
     public boolean conStatus(){
    	 return UaClient.getInstance().conStatus();
     }
     	     
}
