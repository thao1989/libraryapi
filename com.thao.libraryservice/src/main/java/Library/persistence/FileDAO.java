package Library.persistence;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import Library.model.File;
import Library.model.Filelist;

public class FileDAO {
	public Filelist getListOfFile() throws IOException{
		return searchFile(null,null);
	}
	
	public Filelist searchForFile(String sbw, String sbt) throws IOException{
		return searchFile(sbw,sbt);
	}
	
	private Filelist searchFile(String filterByWriter, String filterByTittle) throws IOException {
		Filelist bL= new Filelist();
		FileInputStream fstream = new FileInputStream("Z:/BOOKS.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		
		ArrayList<File> fileList=new ArrayList<File>();
		
		while((strLine=br.readLine())!=null){
			String[] split= strLine.split(",");
			if((filterByWriter == null || split[1].trim().equals(filterByWriter)) &&
			   (filterByTittle==null||split[0].trim().equals(filterByTittle))){
				
				File b= new File();
				b.setTitle(split[0]);
				b.setWriter(split[1]);									
				fileList.add(b);				
			}
		}	
		br.close();
		bL.setFileList(fileList);
		return bL;
	}
}
