package Library.persistence;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import Library.model.User;
import Library.model.Userlist;

@Repository
public class UserDAO {
	@Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String SQL = "SELECT tbl_user.user_id, tbl_user.user_name, tbl_user.address, tbl_user.email, tbl_user.password, tbl_role.role_name"
    		+ " from tbl_user_role "
    		+ "inner join tbl_user on tbl_user.user_id=tbl_user_role.user_id "
    		+ "inner join tbl_role on tbl_role.role_id=tbl_user_role.role_id";
    
    public Userlist getListOfUsers(){
      	 ArrayList<User> users = new ArrayList<User>();
      	 
           List<Map<String, Object>> rows = jdbcTemplate.queryForList(SQL);

           for (Map<String, Object> row : rows) {
          	
                User u = new User();   
                u.setUserID((String)row.get("user_id"));
                u.setUserName((String)row.get("user_name"));
                u.setPassword((String)row.get("password"));
                u.setAddress((String)row.get("address"));
                u.setEmail((String)row.get("email"));
                u.setRole((String)row.get("role_name"));
                users.add(u);      	 
            }       
           Userlist ul=new Userlist();
           ul.setUserList(users);
          return ul;
       }
    
    public Userlist userLogin(String lid, String lpw){
     	 ArrayList<User> users = new ArrayList<User>();
     	 String lg="SELECT tbl_user.user_id,user_name,password, tbl_user_role.role_id "
     	 		+ "from tbl_user inner join tbl_user_role "
     	 		+ "on tbl_user.user_id=tbl_user_role.user_id where tbl_user.user_id='"+ lid +"' and tbl_user.password='"+ lpw +"'";
          List<Map<String, Object>> rows = jdbcTemplate.queryForList(lg);

          for (Map<String, Object> row : rows) {
         	
               User u = new User();   
               u.setUserID((String)row.get("user_id"));
               u.setUserName((String)row.get("user_name"));
               u.setPassword((String)row.get("password"));
               u.setRole((String)row.get("role_id"));
               users.add(u);      	 
           }       
          Userlist ul=new Userlist();
          ul.setUserList(users);
         return ul;
      }
    
    

}
