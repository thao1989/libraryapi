The persistence layer provides data from external data sources (like DB or Filesystem)
and send the data to the service layer.