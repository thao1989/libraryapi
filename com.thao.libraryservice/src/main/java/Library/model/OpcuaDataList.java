package Library.model;

import java.util.ArrayList;

public class OpcuaDataList {
	
	private ArrayList<OpcuaData> opcuaDataList;

	public ArrayList<OpcuaData> getOpcuaDataList() {
		return opcuaDataList;
	}

	public void setOpcuaDataList(ArrayList<OpcuaData> opcuaDataList) {
		this.opcuaDataList = opcuaDataList;
	}

}
