package Library.model;

import java.util.ArrayList;

public class Booklist {
	
	private String errorMsg=null;
	
	private ArrayList<Book> bookList;

	public ArrayList<Book> getBookList() {
		return bookList;
	}

	public void setBookList(ArrayList<Book> bookList) {
		this.bookList = bookList;
	}
	
	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	

}
