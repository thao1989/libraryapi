package Library.model;

import java.util.ArrayList;

public class Filelist {
	private ArrayList<File> fileList;

	public ArrayList<File> getFileList() {
		return fileList;
	}

	public void setFileList(ArrayList<File> fileList) {
		this.fileList = fileList;
	}

}
