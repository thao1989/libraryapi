package Library.service;

import java.io.IOException;

import Library.model.Commentlist;

public interface ICommentService {
	public Commentlist getListOfComments () throws IOException;
	public void insertComments(String iuid, String ic) throws IOException;
	

}
