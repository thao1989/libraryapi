package Library.service;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;


import Library.model.Filelist;
import Library.persistence.FileDAO;

public class FileServceImpl implements IFileService {
	@Autowired
	private FileDAO fileDAO;
	
	public Filelist getListOfFile() throws IOException{
		Filelist bl = fileDAO.getListOfFile();
		return bl;
				
	}
	
	public Filelist searchForFile(String sbw, String sbt) throws IOException{
		Filelist bl= fileDAO.searchForFile(sbw, sbt);
		return bl;
	}
	

}
