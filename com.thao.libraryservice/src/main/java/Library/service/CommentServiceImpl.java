package Library.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import Library.model.Commentlist;
import Library.persistence.CommentDAO;
@Component
public class CommentServiceImpl implements ICommentService {
	
	@Autowired
	private CommentDAO comDAO;

	public Commentlist getListOfComments() throws IOException{
		Commentlist cl= comDAO.getListOfComments();
		return cl;		
	}
	
	public void insertComments(String iuid, String ic) throws IOException{
		comDAO.insertComments(iuid, ic);
	}
	

}
