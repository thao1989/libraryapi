package Library.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import Library.UaClient;
import Library.model.Booklist;
import Library.model.OpcuaDataList;
import Library.persistence.BookDAO;

@Component
public class BookServiceImpl implements IBookService {
	
	@Autowired
	private BookDAO libDAO;
		
	
	public Booklist getListOfBooks() {
		
		Booklist bl = libDAO.getListOfBooks();	
		bl.setErrorMsg("success");
		bl.getErrorMsg();
		int s= libDAO.bookTypes().getOpcuaDataList().size();			
		try {
			UaClient.getInstance().write(libDAO.sumOfBooks(), 2, "Library/Books/Books");
			UaClient.getInstance().write(libDAO.typesOfBooks(), 2, "Library/Books/Types");			
			
			for(int i=0; i<s; i++){
				int quantity =libDAO.bookTypes().getOpcuaDataList().get(i).getQuantity();
				String type = libDAO.bookTypes().getOpcuaDataList().get(i).getType();				
				UaClient.getInstance().write(quantity, 2, "Library/Books/"+type+"");									
			}
				
			
		} catch (Exception e) {			
			e.printStackTrace();						
			bl.setErrorMsg(e.getMessage());
			bl.getErrorMsg();			
		}	
		
		return bl;				
	}
	
	public Booklist searchForBooks(String sbw, String sbt, String sbid, Integer sbq, String sbtp) throws IOException{
		Booklist bl= libDAO.searchForBooks(sbw, sbt, sbid, sbq, sbtp);
		return bl;
	}
	
	public void insertBooks(String isbw, String isbt, String isbid, Integer isbq, String isbtp) throws IOException{
		libDAO.insertBooks(isbid,isbw, isbt, isbtp,isbq);
//		try {
//			Client.getInstance().history(5, "Counter1");
//		} catch (Exception e) {
//			
//			e.printStackTrace();
//		}
	}
	
	public void deleteBooks(String dbid, String dbt, String dbw, String dbtp, Integer dbq) throws IOException{
		libDAO.deleteBooks(dbid, dbt, dbw, dbtp, dbq);	
//		try {
//			Client.getInstance().history(2, "Library/Books/Books");
//		} catch (Exception e) {
//			
//			e.printStackTrace();
//		}
	}
	
	public void updateBooks(String id, String ubt, String ubw, String ubtp, Integer ubq) throws Exception{
		libDAO.updateBooks(id, ubt, ubw, ubtp, ubq);					
	}
	
	public Booklist searchLibrary(String kw) throws IOException{
		Booklist bl= libDAO.searchLibrary(kw);
		return bl;
	}
	
	public Booklist searchType(String st) throws IOException{
		Booklist bl= libDAO.searchType(st);
		return bl;
	}
		
	public OpcuaDataList bookType() throws IOException{
		OpcuaDataList odl= libDAO.bookTypes();
		return odl;
	}
	
	public boolean conStatus(){
		return libDAO.conStatus();
	}
}
