package Library.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import Library.model.Userlist;
import Library.persistence.UserDAO;

@Component
public class UserServiceImpl implements IUserService {
	@Autowired
	private UserDAO userDAO;
	
	public Userlist getListOfUsers() throws IOException{
		Userlist ul = userDAO.getListOfUsers();
		return ul;				
	}
	
	public Userlist userLogin(String lid, String lpw) throws IOException{
		Userlist ul = userDAO.userLogin(lid,lpw);
		return ul;
	}

}
