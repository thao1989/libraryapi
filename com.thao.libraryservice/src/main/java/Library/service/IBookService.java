package Library.service;

import java.io.IOException;

import Library.model.Booklist;
import Library.model.OpcuaDataList;

public interface IBookService {
	public Booklist getListOfBooks() throws IOException;
	public Booklist searchForBooks(String sbw, String sbt, String sbid, Integer sbq, String sbtp) throws IOException;
	public void insertBooks(String isbw, String isbt, String isbid, Integer isbq, String isbtp) throws IOException;
	public void deleteBooks(String dbid, String dbt, String dbw, String dbtp, Integer dbq) throws IOException;
	public void updateBooks(String id, String ubt, String ubw, String ubtp, Integer ubq) throws IOException, Exception;
	public Booklist searchLibrary(String kw) throws IOException;
	public Booklist searchType(String st) throws IOException;	
	public OpcuaDataList bookType() throws IOException;
	public boolean conStatus();
}
