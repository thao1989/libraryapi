package Library.service;

import java.io.IOException;
import Library.model.Userlist;

public interface IUserService {
	public Userlist getListOfUsers() throws IOException;
	public Userlist userLogin(String lid, String lpw) throws IOException;
}
