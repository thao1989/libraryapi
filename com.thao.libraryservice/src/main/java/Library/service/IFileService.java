package Library.service;
import java.io.IOException;

import Library.model.Filelist;

public interface IFileService {
	public Filelist getListOfFile() throws IOException;
	public Filelist searchForFile(String sbw, String sbt) throws IOException;

}
